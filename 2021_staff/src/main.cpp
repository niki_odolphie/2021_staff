#include <Arduino.h>
#define FASTLED_INTERNAL
#include <FastLED.h>

//LED Setup
#define LED_PIN 7
#define COLOR_ORDER GRB
#define CHIPSET WS2812B
#define BRIGHTNESS 255
#define FRAMES_PER_SECOND 120
#define NUM_LEDS (300)
CRGB leds[NUM_LEDS];

// Some re-usable variables
CRGBPalette16 currentPalette = PartyColors_p;
TBlendType currentBlending;
uint8_t gHue = 0;
int randomPallet = 0;

//Include some code
#include <utilityfunctions.h>
#include <playlist.h> //Update this with any new patterns you add
#include <playlist_controller.h>
#include <RandomisePallet.h>

void setup()
{
  delay(500); //Give the board some time to wake up
  FastLED.setMaxPowerInVoltsAndMilliamps(5, 200); //Reduce last number to make batteries last longer but dimmer
    FastLED.addLeds<1, CHIPSET, LED_PIN, COLOR_ORDER>(leds, NUM_LEDS);
  LEDS.setBrightness(BRIGHTNESS);

  RestartPlaylist();
}

void loop()
{
  gPlaylist[gCurrentTrackNumber].mPattern();
  FastLED.show();
  delay(1000 / FRAMES_PER_SECOND); //Stops animations from running too quick - think Turbo button from a 486 DX2.
    FastLED.setMaxRefreshRate(100); //LED matrix seems to just about cope with this speed


  EVERY_N_MILLISECONDS(20) { gHue++; }
  EVERY_N_MILLISECONDS(2000) { RandomisePallet(); }

  EVERY_N_SECONDS_I(patternTimer, gPlaylist[gCurrentTrackNumber].mTime)
  {
    nextPattern();
    patternTimer.setPeriod(gPlaylist[gCurrentTrackNumber].mTime);

    if (gRestartPlaylistFlag)
    {
      gCurrentTrackNumber = 0;
      patternTimer.setPeriod(gPlaylist[gCurrentTrackNumber].mTime);
      patternTimer.reset();
      gRestartPlaylistFlag = false;
    }
  }
}
