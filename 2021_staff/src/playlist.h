
typedef void (*SimplePattern)();
typedef SimplePattern SimplePatternList[];
typedef struct { SimplePattern mPattern;  uint16_t mTime; } PatternAndTime;
typedef PatternAndTime PatternAndTimeList[];

#include "patterns/WhiteMove.h"
#include "patterns/Rainbow.h"
#include "patterns/Juggle.h"
#include "patterns/StripedPallet.h"
#include "patterns/Cylon.h"
#include "patterns/RainbowCentre.h"
#include "patterns/RunningLights.h"
#include "patterns/Sparkle.h"
#include "patterns/Sinelon.h"
#include "patterns/RGBLoop.h"
#include "patterns/PatternMaker.h"
#include "patterns/RandomFades.h"
#include "patterns/RainbowBeat.h"
#include "patterns/Fire.h"
#include "patterns/Flow.h"
#include "patterns/BlurWaves.h"
#include "patterns/SplitStaff.h"

uint16_t timePerPatternShort = 8;
uint16_t timePerPatternMedium = 12;
uint16_t timePerPatternLong = 16;

const PatternAndTimeList gPlaylist = { 
  //  {RunningLights,1}, // TODO Runs for too long
{SplitStaff,3},
{Sparkle,timePerPatternShort},
    {Rainbow,timePerPatternShort},
  
  {RGBLoop,timePerPatternShort},
  {RainbowCentre,timePerPatternShort},
  {BlurWaves,timePerPatternShort},
  {RandomFades,timePerPatternShort},
  {Fire,timePerPatternShort},
  {Flow,timePerPatternShort},
  {WhiteMove,timePerPatternShort},
  {RainbowBeat,timePerPatternLong},
  {Sparkle,timePerPatternShort},
  {PatternMaker,timePerPatternShort},
  {Cylon,timePerPatternShort},
  {StripedPallet,timePerPatternLong},
  {Sinelon,timePerPatternShort},
  {Juggle,timePerPatternLong}
};
