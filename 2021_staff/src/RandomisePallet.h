void RandomisePallet(){
	randomPallet = random(0,27);

	if( randomPallet == 0)   { fill_solid( currentPalette, 16, CRGB::Red); }
    if( randomPallet == 1)   { fill_solid( currentPalette, 16, CRGB::Crimson); }
    if( randomPallet == 2)   { fill_solid( currentPalette, 16, CRGB::Orange); }
    if( randomPallet == 3)   { fill_solid( currentPalette, 16, CRGB::DarkOrange); }
    if( randomPallet == 4)   { fill_solid( currentPalette, 16, CRGB::Yellow); }
    if( randomPallet == 5)   { fill_solid( currentPalette, 16, CRGB::Gold); }
    if( randomPallet == 6)   { fill_solid( currentPalette, 16, CRGB::Green); }
    if( randomPallet == 7)   { fill_solid( currentPalette, 16, CRGB::GreenYellow); }
    if( randomPallet == 8)   { fill_solid( currentPalette, 16, CRGB::Lime); }  
    if( randomPallet == 9)   { fill_solid( currentPalette, 16, CRGB::Blue); }  
    if( randomPallet == 10)  { fill_solid( currentPalette, 16, CRGB::Aqua); }  
    if( randomPallet == 11)  { fill_solid( currentPalette, 16, CRGB::DarkTurquoise); }
    if( randomPallet == 12)  { fill_solid( currentPalette, 16, CRGB::Navy); }
    if( randomPallet == 13)  { fill_solid( currentPalette, 16, CRGB::Purple); }        
    if( randomPallet == 14)  { fill_solid( currentPalette, 16, CRGB::Violet); }    
    if( randomPallet == 15)  { fill_solid( currentPalette, 16, CRGB::DeepPink); }
    if( randomPallet == 16)  { fill_solid( currentPalette, 16, CRGB::LightCoral); }
    if( randomPallet == 17)  { fill_solid( currentPalette, 16, CRGB::White); }
    if( randomPallet == 18)  { fill_solid( currentPalette, 16, CRGB::BurlyWood); }
    if( randomPallet == 19)  { fill_solid( currentPalette, 16, CRGB::Thistle); }  
    if( randomPallet == 20)  { fill_solid( currentPalette, 16, CRGB::White); }    
    if( randomPallet == 21)  { currentPalette = RainbowColors_p; }
    if( randomPallet == 22)  { currentPalette = RainbowStripeColors_p; }
    if( randomPallet == 23)  { currentPalette = OceanColors_p; }
    if( randomPallet == 24)  { currentPalette = CloudColors_p; }
    if( randomPallet == 25)  { currentPalette = LavaColors_p; }
    if( randomPallet == 26)  { currentPalette = ForestColors_p; }
    if( randomPallet == 27)  { currentPalette = PartyColors_p; }
}