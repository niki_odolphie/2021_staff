void WhiteMove()
{
   ClearLEDS();

   for (int whiteLed = NUM_LEDS; whiteLed > 0; whiteLed = whiteLed - 1)
   {
      leds[whiteLed] = CHSV(gHue++, 255, 255);
      FastLED.show();
      fadeToBlackBy(leds, NUM_LEDS, 30);
   }
   
   for (int whiteLed = 0; whiteLed < NUM_LEDS; whiteLed = whiteLed + 2)
   {
      leds[whiteLed] = CRGB::White;
      FastLED.show();
      fadeToBlackBy(leds, NUM_LEDS, 30);
   }

}