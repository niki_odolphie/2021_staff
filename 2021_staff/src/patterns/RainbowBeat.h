void RainbowBeat()
{
	// colored stripes pulsing at a defined Beats-Per-Minute (BPM)
	uint8_t BeatsPerMinute = 62; // Rate of beat
	
	uint8_t beat = beatsin8(BeatsPerMinute, 64, 255); // Beat advances and retreats in a sine wave
	for (int i = 0; i < NUM_LEDS; i++)
	{
		leds[i] = ColorFromPalette(currentPalette, gHue + (i * 2), beat - gHue + (i * 60));
	}

}