void BlurWaves()
{
    int sinBeat = beatsin16(8, 0, NUM_LEDS - 1);
    int sinBeat2 = beatsin16(9, 0, NUM_LEDS - 1);
    int sinBeat3 = beatsin16(10, 0, NUM_LEDS - 1);

    leds[sinBeat] = CRGB::Green;
    leds[sinBeat2] = CRGB::Blue;
    leds[sinBeat3] = CRGB::Red;

    blur1d(leds, NUM_LEDS, 50);

    fadeToBlackBy(leds, NUM_LEDS, 10);

    //FastLED.show();
}