void StripedPallet()
{
    currentPalette = PartyColors_p;
    static uint8_t startIndex = 0;
    startIndex = startIndex + 12; /* higher = faster motion */

    fill_palette(leds, NUM_LEDS,
                 startIndex, 6, /* higher = narrower stripes */
                 currentPalette, 255, LINEARBLEND);

    FastLED.delay(2000 / 30);
}
