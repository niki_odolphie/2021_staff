
void Cylon()
{
	// First slide the led in one direction
	for(int i = 0; i < NUM_LEDS; i++) {
		leds[i] = CHSV(gHue++, 255, 255);
		FastLED.show(); 
		fadeToBlackBy(leds, NUM_LEDS, 20);
	}

	// Now go in the other direction.  
	for(int i = (NUM_LEDS)-1; i >= 0; i--) {
		leds[i] = CHSV(gHue++, 255, 255);
		FastLED.show();
        fadeToBlackBy(leds, NUM_LEDS, 20);
	}
}
