//Yaroslaw Turbin,
//https://vk.com/ldirko
//https://www.reddit.com/user/ldirko/

// effect for 50 led strip https://editor.soulmatelights.com/gallery/392-flow-led-stripe

void Flow() {
const float hl = NUM_LEDS/1.3;   
static byte hue=0;


int t = millis()/20;
for (int i = 0; i <NUM_LEDS; i++) {
 int c = (abs(i - hl)/hl)*127;

  c = sin8(c);
  c = sin8(c/2+ t);
  byte b = sin8(c+t/8); 

 leds [i] = CHSV(b+hue,255,255);

 }
 

//blur1d(leds,N_LEDS,32 );
}