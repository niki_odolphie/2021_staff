void RandomFades()
{
	uint16_t pos;
	pos = random16(0, NUM_LEDS - 1);
	if (CRGB(0, 0, 0) == CRGB(leds[pos]))
	{
		leds[pos] = CHSV((random8() % 256), 200, 255);
	}
	fadeToBlackBy(leds, NUM_LEDS, 16);
}