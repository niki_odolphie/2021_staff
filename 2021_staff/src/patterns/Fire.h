void Fire() //From https://editor.soulmatelights.com/gallery/428-fire-flow
{
    const float hl = NUM_LEDS / 15;

    int t = millis() / 5;

    for (int i = 0; i < NUM_LEDS; i++)
    {
        int c = (abs(i - hl) / hl) * 30;
        byte b = sin8(c + t / 8);
        leds[i] = ColorFromPalette(LavaColors_p, (b + t) % 255, 255);
    }

}