#include "FastLED.h"

void SplitStaff()
{

    ClearLEDS();

    //Top to middle
    for (int whiteLed = 0; whiteLed < NUM_LEDS / 2; whiteLed = whiteLed + 1)
    {
        leds[whiteLed] = CRGB::Red;
        leds[NUM_LEDS - whiteLed] = CRGB::Green;
        delay(1);
        FastLED.show();
        fadeToBlackBy(leds, NUM_LEDS, 40);
    }

/*     int delayTimer = 0;
    for (int flashcount = 0; flashcount < 25; flashcount++)
    {
        //setAll(255, 255, 255);
        setAll(random8(),random8(),random8());
        FastLED.show();

        setAll(0, 0, 0);
        delay(1);
        FastLED.show();
        delay(delayTimer);
        delayTimer = delayTimer + 8;
    }
     */

    //Middle out
    for (size_t i = 0; i < 3; i++)
    {
        for (int whiteLed = NUM_LEDS / 2; whiteLed > 0; whiteLed = whiteLed - 2)
        {
            leds[whiteLed] = CRGB::White;
            leds[NUM_LEDS - whiteLed] = CRGB::White;
            FastLED.show();
            fadeToBlackBy(leds, NUM_LEDS, 10);
        }
    }
}
