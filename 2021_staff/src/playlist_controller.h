//from https://gist.github.com/kriegsman/841c8cd66ed40c6ecaae

uint8_t gCurrentTrackNumber = 0; // Index number of which pattern is current
bool gRestartPlaylistFlag = false;
bool gLoopPlaylist = true;

#define ARRAY_SIZE(A) (sizeof(A) / sizeof((A)[0]))

void nextPattern()
{
  gCurrentTrackNumber = gCurrentTrackNumber + 1;

  if (gCurrentTrackNumber == ARRAY_SIZE(gPlaylist))
  {
    if (gLoopPlaylist == true)
    {
      // restart at beginning
      gCurrentTrackNumber = 0;
    }
    else
    {
      // stay on the last track
      gCurrentTrackNumber--;
    }
  }
}

void RestartPlaylist()
{
  gRestartPlaylistFlag = true;
}
