//Common re-usable functions for the patterns.

void setAll(byte red, byte green, byte blue)
{
  for (int i = 0; i < NUM_LEDS; i++)
  {
    leds[i] = CRGB(red, green, blue);
  }
  FastLED.show();
}

void setPixel(int Pixel, byte red, byte green, byte blue)
{

  leds[Pixel].r = red;
  leds[Pixel].g = green;
  leds[Pixel].b = blue;
}

void ClearLEDS(){
  fill_solid(leds,NUM_LEDS,CRGB::Black);
}